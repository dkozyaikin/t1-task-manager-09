package ru.t1.dkozyaikin.tm.component;

import ru.t1.dkozyaikin.tm.api.ICommandController;
import ru.t1.dkozyaikin.tm.api.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.ICommandService;
import ru.t1.dkozyaikin.tm.controller.CommandController;
import ru.t1.dkozyaikin.tm.repository.CommandRepository;
import ru.t1.dkozyaikin.tm.service.CommandService;

import java.util.Scanner;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.ARG_COMMANDS;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

public class Boostrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private static void exit() {
        System.exit(0);
    }

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (processArguments(args)) exit();
        processCommands();
    }

    private void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processCommand(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            commandController.displayErrorCommand();
            return;
        }
        switch (parameter) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displayInfo();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_EXIT:
                exit();
            default: {
                commandController.displayErrorCommand();
            }
        }
    }

    private boolean processArguments(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            commandController.displayErrorCommand();
            return;
        }
        switch (parameter) {
            case ARG_HELP:
                commandController.displayHelp();
                break;
            case ARG_VERSION:
                commandController.displayVersion();
                break;
            case ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ARG_INFO:
                commandController.displayInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.displayArguments();
                break;
            case ARG_COMMANDS:
                commandController.displayCommands();
                break;
            default: {
                commandController.displayErrorCommand();
            }
        }
    }

}
