package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.ICommandRepository;
import ru.t1.dkozyaikin.tm.model.Command;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.ARG_INFO;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

public class CommandRepository implements ICommandRepository {

    public static Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION,
            "Display app version."
    );

    public static Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT,
            "Display developer info."
    );

    public static Command HELP = new Command(
            CMD_HELP, ARG_HELP,
            "Display app commands."
    );

    public static Command EXIT = new Command(
            CMD_EXIT, null,
            "Close app."
    );

    public static Command INFO = new Command(
            CMD_INFO, ARG_INFO,
            "Display system info."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, EXIT, HELP, ABOUT, VERSION
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}

