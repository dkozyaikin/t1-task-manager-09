package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
