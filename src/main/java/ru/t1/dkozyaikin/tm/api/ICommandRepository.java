package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
