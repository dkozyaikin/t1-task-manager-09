package ru.t1.dkozyaikin.tm.controller;

import ru.t1.dkozyaikin.tm.api.ICommandController;
import ru.t1.dkozyaikin.tm.api.ICommandService;
import ru.t1.dkozyaikin.tm.model.Command;
import ru.t1.dkozyaikin.tm.util.FormatUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("Task Manager started");
    }

    @Override
    public void displayErrorCommand() {
        System.out.println("Error! This command not supported...");
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Denis Kozyaykin");
        System.out.println("dkozyaikin@t1-consulting.ru");
    }

    @Override
    public void displayInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
